package cagin.com.bitcoingraph.feature

import androidx.lifecycle.ViewModelProvider
import cagin.com.bitcoingraph.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class GraphActivityModule {
    @Provides
    fun provideGraphViewModel(graphViewModel: GraphViewModel): ViewModelProvider.Factory {
        return ProjectViewModelFactory(graphViewModel)
    }
}