package cagin.com.bitcoingraph.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import cagin.com.bitcoingraph.R
import cagin.com.bitcoingraph.common.ActivityUtil
import cagin.com.bitcoingraph.common.Constants
import cagin.com.bitcoingraph.common.DateUtil
import cagin.com.bitcoingraph.data.retrofit.model.BitcoinModel
import cagin.com.bitcoingraph.data.retrofit.model.XYValues
import cagin.com.bitcoingraph.databinding.ActivityGraphBinding
import com.jjoe64.graphview.series.LineGraphSeries
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import dagger.android.AndroidInjection
import timber.log.Timber
import javax.inject.Inject
import kotlin.collections.ArrayList

//TODO even if use isScrollable method to the graph, it is not scrollable.
//TODO unit test, espresso test

class GraphActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGraphBinding
    private lateinit var viewModel: GraphViewModel
    private var xyValueList: ArrayList<XYValues>? = null
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        Timber.d("onCreate")
        super.onCreate(savedInstanceState)
        initBinding()
        if (savedInstanceState == null)
            sendBitcoinRequest()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        Timber.d("restore after rotation.")
        xyValueList = savedInstanceState?.getParcelableArrayList(Constants.Bundle.XY_LIST)
        super.onRestoreInstanceState(savedInstanceState)
        setGraph(xyValueList?.subList(0, 10))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Timber.d("state is saving.")
        outState?.putParcelableArrayList(Constants.Bundle.XY_LIST, xyValueList)
        super.onSaveInstanceState(outState)
    }

    private fun sendBitcoinRequest() {
        viewModel.sendBitcoinRequest().observe(this,
            Observer<BitcoinModel> { networkResponse ->
                if (networkResponse != null) {
                    xyValueList = networkResponse.values as ArrayList<XYValues>
                    setGraph(xyValueList?.subList(0, 10))
                } else {
                    Timber.w("connection failed.")
                    ActivityUtil.displaySnack(
                        "Please check your internet connection!",
                        binding.constraintRoot
                    )
                }
            })
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_graph)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(GraphViewModel::class.java)
    }

    private fun setGraph(graphValues: List<XYValues>?) {
        Timber.d("graph is setting.")
        val series2 = LineGraphSeries<DataPoint>()
        if (graphValues != null) {
            for (graphValue: XYValues in graphValues) {
                Timber.i("Graph Value, %d, %d", graphValue.x, graphValue.y)
                series2.appendData(DataPoint(DateUtil.getDate(graphValue.x), graphValue.y), true, 10, false)
            }
        }
        binding.graph.gridLabelRenderer.labelFormatter = DateAsXAxisLabelFormatter(this)
        binding.graph.gridLabelRenderer.numHorizontalLabels = 3
        binding.graph.viewport.isScrollable = true
        binding.graph.addSeries(series2)
    }
}
