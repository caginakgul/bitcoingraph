package cagin.com.bitcoingraph.feature

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cagin.com.bitcoingraph.common.ConnectionUtil
import cagin.com.bitcoingraph.data.repository.BitcoinRepository
import cagin.com.bitcoingraph.data.retrofit.model.BitcoinModel
import javax.inject.Inject

class GraphViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    lateinit var bitcoinRepository: BitcoinRepository

    fun sendBitcoinRequest(): LiveData<BitcoinModel> {
        if (ConnectionUtil.isConnectedOrConnecting(getApplication())) {
            return bitcoinRepository.sendBitcoinRequest()
        } else {
            val liveData: MutableLiveData<BitcoinModel> = MutableLiveData()
            liveData.value = null
            return liveData
        }
    }
}