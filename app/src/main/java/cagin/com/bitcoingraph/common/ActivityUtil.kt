package cagin.com.bitcoingraph.common


import android.view.View
import com.google.android.material.snackbar.Snackbar


class ActivityUtil {
    companion object {
        fun displaySnack(message: String, view: View){
            Snackbar.make(
                view, // Parent view
                message, // Message to show
                Snackbar.LENGTH_SHORT // How long to display the message.
            ).show()
        }
    }
}