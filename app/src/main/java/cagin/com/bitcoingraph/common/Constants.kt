package cagin.com.bitcoingraph.common

class Constants {
    object Api {
        val BASE_URL = "https://api.blockchain.info/charts/"
    }
    object Bundle {
        val XY_LIST = "XY_VALUES"
    }
}