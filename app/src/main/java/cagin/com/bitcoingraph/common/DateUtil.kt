package cagin.com.bitcoingraph.common

import java.util.*

class DateUtil {
    companion object {
        fun getDate(time: Int): Date {
            var calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = time * 1000.toLong()
            return calendar.time
        }
    }
}