package cagin.com.bitcoingraph.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class ConnectionUtil {
    companion object {
        fun isConnectedOrConnecting(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            return activeNetwork?.isConnected == true
        }
    }
}