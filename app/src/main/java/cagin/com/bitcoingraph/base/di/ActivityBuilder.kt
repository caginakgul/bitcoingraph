package cagin.com.bitcoingraph.base.di


import cagin.com.bitcoingraph.feature.GraphActivity
import cagin.com.bitcoingraph.feature.GraphActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [GraphActivityModule::class])
    internal abstract fun contributeGraphActivity(): GraphActivity
}