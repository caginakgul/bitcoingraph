package cagin.com.bitcoingraph.base.di

import cagin.com.bitcoingraph.BitcoinGraphApplication
import dagger.Module

@Module
class AppModule(val app: BitcoinGraphApplication)