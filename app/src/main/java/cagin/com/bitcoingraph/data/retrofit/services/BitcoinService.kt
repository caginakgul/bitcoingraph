package cagin.com.bitcoingraph.data.retrofit.services

import cagin.com.bitcoingraph.data.retrofit.model.BitcoinModel
import retrofit2.Call
import retrofit2.http.GET

interface BitcoinService {
    @GET("transactions-per-second?timespan=5weeks&rollingAverage=8hours&format=json")
    fun sendBitcoinRequest(): Call<BitcoinModel>
}