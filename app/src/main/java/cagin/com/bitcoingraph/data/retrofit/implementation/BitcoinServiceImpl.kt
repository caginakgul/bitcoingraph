package cagin.com.bitcoingraph.data.retrofit.implementation

import cagin.com.bitcoingraph.data.retrofit.model.BitcoinModel
import cagin.com.bitcoingraph.data.retrofit.services.BitcoinService
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

class BitcoinServiceImpl @Inject constructor(builder: Retrofit.Builder) : BitcoinService {
   private val bitcoinService: BitcoinService = builder.build().create(BitcoinService::class.java)

    override fun sendBitcoinRequest(): Call<BitcoinModel> {
       return bitcoinService.sendBitcoinRequest()
    }
}