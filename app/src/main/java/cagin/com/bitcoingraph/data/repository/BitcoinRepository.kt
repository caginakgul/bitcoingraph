package cagin.com.bitcoingraph.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cagin.com.bitcoingraph.data.retrofit.implementation.BitcoinServiceImpl
import cagin.com.bitcoingraph.data.retrofit.model.BitcoinModel
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class BitcoinRepository @Inject constructor(bitcoinServiceImpl: BitcoinServiceImpl) {
    private val bitcoinServiceImplementation: BitcoinServiceImpl = bitcoinServiceImpl

    fun sendBitcoinRequest(): LiveData<BitcoinModel> {
        val liveData: MutableLiveData<BitcoinModel> = MutableLiveData()
        bitcoinServiceImplementation.sendBitcoinRequest().enqueue((object : retrofit2.Callback<BitcoinModel> {
            override fun onFailure(call: Call<BitcoinModel>?, t: Throwable?) {
                Timber.e( t.toString())
                liveData.value = null
            }
            override fun onResponse(call: Call<BitcoinModel>?, response: Response<BitcoinModel>?) {
                Timber.i(response?.message())
                liveData.value = response?.body()
            }
        }))
        return liveData
    }
}