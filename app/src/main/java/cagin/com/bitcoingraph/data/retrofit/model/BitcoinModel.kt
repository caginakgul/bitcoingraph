package cagin.com.bitcoingraph.data.retrofit.model

data class BitcoinModel (
    val status: String,
    val name: String,
    val unit: String,
    val period: String,
    val description: String,
    val values: List<XYValues>
)