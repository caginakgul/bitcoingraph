package cagin.com.bitcoingraph.data.retrofit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class XYValues (
    val x: Int,
    val y: Double
) : Parcelable